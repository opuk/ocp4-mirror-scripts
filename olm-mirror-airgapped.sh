#!/bin/bash

#export OC="oc"
export OCP_RELEASE="4.10"
#export ARCHITECTURE="x86_64"
#export LOCAL_REG='localhost:5000'
#export LOCAL_REPO='ocp4/openshift4'
#export LOCAL_REG_INSEC='true'
#export UPSTREAM_REPO='openshift-release-dev'
#export OCP_ARCH="x86_64"

export RH_OP_INDEX="registry.redhat.io/redhat/redhat-operator-index:v${OCP_RELEASE}"
export LOCAL_SECRET_JSON="${HOME}/pull-secret"

export REMOVABLE_MEDIA_PATH="/opt/data/ocp/redhat-operators-${OCP_RELEASE}-$(date -I)"

mkdir -p $REMOVABLE_MEDIA_PATH

cd $REMOVABLE_MEDIA_PATH

oc adm catalog mirror \
  --dir=$REMOVABLE_MEDIA_PATH \
  $RH_OP_INDEX \
  --index-filter-by-os="linux/amd64" \
  file:///local/index \
  -a ${LOCAL_SECRET_JSON}

