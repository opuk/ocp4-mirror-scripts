#!/bin/bash

export OCP_VERSION=4.10
export OCP_RELEASE=4.10.4
export OCP_ZSTREAM=4.10.4
export LOCAL_REGISTRY='registry.example.com:8443'
export LOCAL_REPOSITORY='ocp4'
export PRODUCT_REPO='openshift-release-dev'
export LOCAL_SECRET_JSON="$HOME/pull-secret"
export RELEASE_NAME="ocp-release"
export ARCHITECTURE=x86_64
export REMOVABLE_MEDIA_PATH=/opt/data/ocp/ocp-${OCP_ZSTREAM}-$(date -I)

mkdir -p ${REMOVABLE_MEDIA_PATH}

#oc adm release mirror -a ${LOCAL_SECRET_JSON} --to-dir=${REMOVABLE_MEDIA_PATH}/mirror quay.io/${PRODUCT_REPO}/${RELEASE_NAME}:${OCP_RELEASE}-${ARCHITECTURE}
oc adm release mirror -a ${LOCAL_SECRET_JSON} --to-dir=${REMOVABLE_MEDIA_PATH}/mirror quay.io/${PRODUCT_REPO}/${RELEASE_NAME}:${OCP_ZSTREAM}-${ARCHITECTURE}

OCP_RELEASE_NUMBER=$OCP_ZSTREAM

ARCHITECTURE=x86_64
DIGEST="$(oc adm release info quay.io/openshift-release-dev/ocp-release:${OCP_RELEASE_NUMBER}-${ARCHITECTURE} | sed -n 's/Pull From: .*@//p')"

DIGEST_ALGO="${DIGEST%%:*}"
DIGEST_ENCODED="${DIGEST#*:}"
SIGNATURE_BASE64=$(curl -s "https://mirror.openshift.com/pub/openshift-v4/signatures/openshift/release/${DIGEST_ALGO}=${DIGEST_ENCODED}/signature-1" | base64 -w0 && echo)
cat > ${REMOVABLE_MEDIA_PATH}/checksum-${OCP_RELEASE_NUMBER}.yaml <<EOF
apiVersion: v1
kind: ConfigMap
metadata:
  name: release-image-${OCP_RELEASE_NUMBER}
  namespace: openshift-config-managed
  labels:
    release.openshift.io/verification-signatures: ""
binaryData:
  ${DIGEST_ALGO}-${DIGEST_ENCODED}: ${SIGNATURE_BASE64}
EOF

echo "oc adm upgrade --allow-explicit-upgrade --to-image ${LOCAL_REGISTRY}/${LOCAL_REPOSITORY}@sha256:${DIGEST_ENCODED}" > ${REMOVABLE_MEDIA_PATH}/apply-update.sh

echo "oc image mirror --from-dir=\${PWD}/mirror 'file://openshift/release:${OCP_ZSTREAM}*' ${LOCAL_REGISTRY}/${LOCAL_REPOSITORY}" >  ${REMOVABLE_MEDIA_PATH}/import.sh

chmod 700 ${REMOVABLE_MEDIA_PATH}/apply-update.sh

